package com.example.EduAssistant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EduAssistantApplication {

	private static final Logger logger= LoggerFactory.getLogger(EduAssistantApplication.class);
	public static void main(String[] args) {
		SpringApplication.run(EduAssistantApplication.class, args);
	}

}
