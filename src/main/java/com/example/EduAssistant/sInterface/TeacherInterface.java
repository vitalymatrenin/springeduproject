package com.example.EduAssistant.sInterface;

//import com.example.EduAssistant.dto.ResponseObject;
import com.example.EduAssistant.dto.PageAndSortDTO;
import com.example.EduAssistant.dto.teacher.TeacherGetDTO;
import com.example.EduAssistant.dto.teacher.TeacherSaveDTO;

import java.util.List;
import java.util.UUID;

public interface TeacherInterface {
    public UUID save(TeacherSaveDTO teacher);
    public TeacherGetDTO findById(UUID id);
    public List<UUID> save(List<TeacherSaveDTO> teacher);
    public List<TeacherGetDTO> findByIdList(List<UUID> uuidList);
    public List<TeacherGetDTO> findAll(PageAndSortDTO pageAndSortDTO);
    public List<TeacherGetDTO> findAllLazy();
    public void deleteById(UUID uuid);
    public void update(TeacherSaveDTO teacherSaveDTO);
    public List<TeacherGetDTO> findAllBySubject(String subject);
}
