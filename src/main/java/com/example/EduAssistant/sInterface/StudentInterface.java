package com.example.EduAssistant.sInterface;

//import com.example.EduAssistant.dto.ResponseObject;
import com.example.EduAssistant.dto.PageAndSortDTO;
import com.example.EduAssistant.dto.student.StudentGetDTO;
import com.example.EduAssistant.dto.student.StudentSaveDTO;
import com.example.EduAssistant.entity.Student;

import java.util.List;
import java.util.UUID;

public interface StudentInterface {
    public UUID save(StudentSaveDTO student);
    public StudentGetDTO findById(UUID id);
    public List<UUID> save(List<StudentSaveDTO> student);
    public List<StudentGetDTO> findByIdList(List<UUID> uuidList);
    public List<StudentGetDTO> findAll(PageAndSortDTO pageAndSortDTO);
    public List<StudentGetDTO> findAllLazy();
    public void deleteById(UUID uuid);
    public void update(StudentSaveDTO studentPutSaveDTO);
    public List<StudentGetDTO> findAllExcellentStudents();
    public List<StudentGetDTO> findAllUnderachievingStudents();
}
