package com.example.EduAssistant.sInterface;

import com.example.EduAssistant.dto.Grade.GradeDTO;
import com.example.EduAssistant.dto.PageAndSortDTO;

import java.util.List;
import java.util.UUID;


public interface GradeInterface {

    public GradeDTO findById(UUID id);
    public List<GradeDTO> findAll(PageAndSortDTO pageAndSortDTO);
    public UUID save(GradeDTO saveGradeDTO);

    public void update(GradeDTO updateGradeDTO) ;
    public void deleteById(UUID id);
    public List<GradeDTO> findAllByGrade(int grade);
    public List<GradeDTO> findAllBySubject(String subject);

}
