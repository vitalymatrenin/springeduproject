package com.example.EduAssistant.repository;

import com.example.EduAssistant.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface TeacherRepository extends JpaRepository<Teacher, UUID> {

    List<Teacher> findDistinctAllByGradesAssigned_SubjectIgnoreCase(String subject);
}
