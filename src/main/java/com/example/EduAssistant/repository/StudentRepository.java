package com.example.EduAssistant.repository;

import com.example.EduAssistant.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface StudentRepository extends JpaRepository<Student, UUID> {


    @Query("SELECT DISTINCT st FROM Student st JOIN st.gradesReceived gr WHERE gr.grade = 5 AND st NOT IN (SELECT s FROM Student s JOIN s.gradesReceived g WHERE g.grade < 5)")
    List<Student> findAllExcellentStudents();

    @Query("SELECT DISTINCT st FROM Student st JOIN st.gradesReceived gr WHERE gr.grade < 4")
    List<Student> findAllUnderachievingStudents();
}
