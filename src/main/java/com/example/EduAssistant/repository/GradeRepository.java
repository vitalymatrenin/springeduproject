package com.example.EduAssistant.repository;

import com.example.EduAssistant.entity.Grade;
import com.example.EduAssistant.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface GradeRepository extends JpaRepository<Grade, UUID> {

    List<Grade> findAllByGrade(int grade);

    List<Grade> findAllBySubjectIgnoreCase(String subject);
}
