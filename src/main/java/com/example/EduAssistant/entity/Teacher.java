package com.example.EduAssistant.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "teachers", schema = "test")
public class Teacher extends Human{
    @OneToMany(mappedBy = "teacherAssigner",cascade = CascadeType.REMOVE)
    private List<Grade> gradesAssigned;
}
