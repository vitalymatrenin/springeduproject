package com.example.EduAssistant.entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.UUID;


@Getter
@Setter
@Entity
@Table(name = "students",schema = "test")
public class Student extends Human{

    @OneToMany(mappedBy="studentReceiver",cascade = CascadeType.REMOVE)
    private List<Grade> gradesReceived;

}
