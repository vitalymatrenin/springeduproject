package com.example.EduAssistant.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

import java.util.UUID;

@Entity
@Getter
@Setter
@Table(name = "studentgrades", schema = "test")
public class Grade extends BaseEntity{
    private Date entryDate;
    private int grade;
    private String subject;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="teacher_id")
    private Teacher teacherAssigner;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="student_id")
    private Student studentReceiver;


}
