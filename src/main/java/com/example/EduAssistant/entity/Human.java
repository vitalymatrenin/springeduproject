package com.example.EduAssistant.entity;

import com.example.EduAssistant.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
public class Human extends BaseEntity {
    private String firstName;
    private String secondName;
    private String lastName;
    private Date dateOfBirth;
    private String login;
    private String password;
}
