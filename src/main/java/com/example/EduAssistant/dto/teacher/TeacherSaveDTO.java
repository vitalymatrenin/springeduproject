package com.example.EduAssistant.dto.teacher;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeacherSaveDTO extends TeacherGetDTO{
    private String login;
    private String password;
}
