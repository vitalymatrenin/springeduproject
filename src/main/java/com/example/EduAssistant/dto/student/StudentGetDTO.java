package com.example.EduAssistant.dto.student;

import lombok.Getter;
import lombok.Setter;
import java.util.Date;
import java.util.UUID;

@Setter
@Getter
public class StudentGetDTO {
    private String firstName;
    private String secondName;
    private String lastName;
    private Date dateOfBirth;
    private UUID id;
}
