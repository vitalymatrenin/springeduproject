package com.example.EduAssistant.dto.student;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StudentSaveDTO extends StudentGetDTO{
    private String login;
    private String password;
}
