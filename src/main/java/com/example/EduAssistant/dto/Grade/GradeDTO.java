package com.example.EduAssistant.dto.Grade;
import com.example.EduAssistant.entity.Student;
import com.example.EduAssistant.entity.Teacher;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public class GradeDTO {
    private UUID id;
    private Date entryDate;
    private int grade;
    private String subject;
    private UUID teacherAssigner;
    private UUID studentReceiver;
}
