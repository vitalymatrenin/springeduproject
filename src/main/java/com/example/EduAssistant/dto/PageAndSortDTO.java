package com.example.EduAssistant.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PageAndSortDTO {
    private int pageSize;
    private int page;
    private String sortFieldName;
}
