package com.example.EduAssistant.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;

@AllArgsConstructor
@Setter
@Getter
public class BasicExceptionInfo {
    private LocalDateTime timestamp;
    private String error;
    private HttpStatus status;
    private String message;



}
