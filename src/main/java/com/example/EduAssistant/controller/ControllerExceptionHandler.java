package com.example.EduAssistant.controller;

import com.example.EduAssistant.dto.BasicExceptionInfo;
//import com.example.EduAssistant.dto.ResponseObject;
import com.example.EduAssistant.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;

@ControllerAdvice
public class ControllerExceptionHandler {
    @ExceptionHandler(value = {
            EdAsException.class})
    public final ResponseEntity<BasicExceptionInfo> handleException(Exception ex, WebRequest request) {
        BasicExceptionInfo basicExceptionInfo = new BasicExceptionInfo(LocalDateTime.now(), ex.getClass().getSimpleName().toString(), HttpStatus.UNPROCESSABLE_ENTITY, ex.getMessage());
        return new ResponseEntity<BasicExceptionInfo>(basicExceptionInfo, basicExceptionInfo.getStatus());
    }
}
