package com.example.EduAssistant.controller;

import com.example.EduAssistant.dto.PageAndSortDTO;
import com.example.EduAssistant.dto.teacher.TeacherGetDTO;
//import com.example.EduAssistant.dto.ResponseObject;
import com.example.EduAssistant.dto.teacher.TeacherSaveDTO;
import com.example.EduAssistant.sInterface.TeacherInterface;
import com.example.EduAssistant.service.TeacherService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/teachers")
public class TeacherController {

    private final TeacherInterface teacherInterface;

    @PostMapping
    public ResponseEntity<?> createTeacher(@RequestBody TeacherSaveDTO teacher) {
        UUID id = teacherInterface.save(teacher);
        return new ResponseEntity<>(teacherInterface.findById(id), HttpStatus.OK);
    }

    @PostMapping("/add-list")
    public ResponseEntity<?> createTeachers(@RequestBody List<TeacherSaveDTO> teacherSaveDTOList) {
        List<TeacherGetDTO> teachersList = teacherInterface.findByIdList(teacherInterface.save(teacherSaveDTOList));
        return new ResponseEntity<>(teachersList, HttpStatus.OK);
    }

    @PostMapping("/get-list")
    public ResponseEntity<?> findAll(@RequestBody PageAndSortDTO pageAndSortDTO) {
        return new ResponseEntity<>(teacherInterface.findAll(pageAndSortDTO), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getTeacherById(@PathVariable UUID id) {
        return new ResponseEntity<TeacherGetDTO>(teacherInterface.findById(id), HttpStatus.OK);
    }

    @GetMapping("/by-subject/{subject}")
    public ResponseEntity<?> findTeachersBySubject(@PathVariable String subject) {
        return new ResponseEntity<>(teacherInterface.findAllBySubject(subject), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteTeacherById(@PathVariable UUID id) {
        teacherInterface.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity<?> update(@RequestBody TeacherSaveDTO teacherSaveDTO) {
        teacherInterface.update(teacherSaveDTO);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
