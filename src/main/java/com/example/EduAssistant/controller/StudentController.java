package com.example.EduAssistant.controller;

import com.example.EduAssistant.dto.PageAndSortDTO;
import com.example.EduAssistant.dto.student.StudentGetDTO;
import com.example.EduAssistant.dto.student.StudentSaveDTO;
import com.example.EduAssistant.sInterface.StudentInterface;
import com.example.EduAssistant.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/students")
public class StudentController {

    private final StudentInterface studentInterface;

    @PostMapping
    public ResponseEntity<?> createStudent(@RequestBody StudentSaveDTO student) {
        UUID id = studentInterface.save(student);
        return new ResponseEntity<>(studentInterface.findById(id), HttpStatus.OK);
    }

    @PostMapping("/add-list")
    public ResponseEntity<?> createStudents(@RequestBody List<StudentSaveDTO> studentSaveDTOList) {
        List<StudentGetDTO> studentsList = studentInterface.findByIdList(studentInterface.save(studentSaveDTOList));
        return new ResponseEntity<List<?>>(studentsList, HttpStatus.OK);
    }

    @PostMapping("/get-list")
    public ResponseEntity<?> findAll(@RequestBody PageAndSortDTO pageAndSortDTO) {
        return new ResponseEntity<>(studentInterface.findAll(pageAndSortDTO), HttpStatus.OK);
    }



    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable UUID id) {
        return new ResponseEntity<StudentGetDTO>(studentInterface.findById(id), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteById(@PathVariable UUID id) {
        studentInterface.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity<?> update(@RequestBody StudentSaveDTO studentSaveDTO) {
        studentInterface.update(studentSaveDTO);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/get-excellent-list")
    public ResponseEntity<?> findExcellentStudents() {
        return new ResponseEntity<>(studentInterface.findAllExcellentStudents(), HttpStatus.OK);
    }

    @GetMapping("/get-underachieved-list")
    public ResponseEntity<?> findUnderachievingStudents() {
        return new ResponseEntity<>(studentInterface.findAllUnderachievingStudents(), HttpStatus.OK);
    }



}
