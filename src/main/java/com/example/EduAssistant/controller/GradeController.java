package com.example.EduAssistant.controller;

import com.example.EduAssistant.dto.Grade.GradeDTO;
import com.example.EduAssistant.dto.PageAndSortDTO;
import com.example.EduAssistant.repository.GradeRepository;
import com.example.EduAssistant.sInterface.GradeInterface;
import com.example.EduAssistant.service.GradeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

//@RequiredArgsConstructor
@RequiredArgsConstructor
@RequestMapping("/grades")
@RestController
public class GradeController {

    private final GradeInterface gradeInterface;


    @GetMapping("/exc")
    public ResponseEntity<?> getExcellentStudents() {
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @PostMapping("/get-list")
    public ResponseEntity<?> findAll(@RequestBody PageAndSortDTO pageAndSortDTO) {
        return new ResponseEntity<>(gradeInterface.findAll(pageAndSortDTO), HttpStatus.OK);
    }


    @GetMapping("/by-grade/{grade}")
    public ResponseEntity<?> findByGrade(@PathVariable int grade) {
        return new ResponseEntity<>(gradeInterface.findAllByGrade(grade), HttpStatus.OK);
    }


    @GetMapping("/by-subject/{subject}")
    public ResponseEntity<?> findByGrade(@PathVariable String subject) {
        return new ResponseEntity<>(gradeInterface.findAllBySubject(subject), HttpStatus.OK);
    }


    @PutMapping
    public ResponseEntity<?> update(@RequestBody GradeDTO gradeDTO) {
        gradeInterface.update(gradeDTO);
        return new ResponseEntity<>(HttpStatus.OK);

    }


    @PostMapping()
    public ResponseEntity<?> createGradeRecord(@RequestBody GradeDTO gradeDTO) {
        UUID tempId = gradeInterface.save(gradeDTO);
        return new ResponseEntity<>(gradeInterface.findById(tempId), HttpStatus.OK);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable UUID id) {
        gradeInterface.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
