package com.example.EduAssistant.configuration;

import com.example.EduAssistant.dto.Grade.GradeDTO;
import com.example.EduAssistant.entity.Grade;
import com.example.EduAssistant.entity.Student;
import com.example.EduAssistant.entity.Teacher;
import com.example.EduAssistant.repository.StudentRepository;
import com.example.EduAssistant.repository.TeacherRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.spi.MappingContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.UUID;


@AllArgsConstructor
@Configuration
public class MapperConfiguration {

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper tempMapper = new ModelMapper();

        return new ModelMapper();
    }
}
