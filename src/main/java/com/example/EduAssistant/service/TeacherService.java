package com.example.EduAssistant.service;

//import com.example.EduAssistant.dto.ResponseObject;

import com.example.EduAssistant.dto.PageAndSortDTO;
import com.example.EduAssistant.dto.teacher.TeacherGetDTO;
import com.example.EduAssistant.dto.teacher.TeacherSaveDTO;
import com.example.EduAssistant.entity.Teacher;
import com.example.EduAssistant.exceptions.EdAsException;
import com.example.EduAssistant.repository.TeacherRepository;
import com.example.EduAssistant.sInterface.TeacherInterface;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Service
public class TeacherService implements TeacherInterface {

    private final ModelMapper mapper;
    private final TeacherRepository teacherRepository;
    private final ValidityChecker validator;


    public List<UUID> save(List<TeacherSaveDTO> teachersList) {

        if ((teachersList == null) || (teachersList.size() == 0)) {
            throw new EdAsException("Пользовательский ввод не распознан как список учителей");
        }
        List<UUID> uuidList = new ArrayList<>();
        Teacher tempTeacher;
        for (TeacherSaveDTO t : teachersList) {
            validator.checkValidityOfTeacherSaveDTO(t);
            tempTeacher = mapper.map(t, Teacher.class);
            teacherRepository.save(tempTeacher);
            uuidList.add(tempTeacher.getId());
        }
        return uuidList;
    }


    public List<TeacherGetDTO> findAllLazy() {
        return mapper.map(teacherRepository.findAll(), new TypeToken<List<TeacherGetDTO>>() {
        }.getType());
    }


    public List<TeacherGetDTO> findAll(PageAndSortDTO pageAndSortDTO) {
        List<Teacher> teacherList = teacherRepository.findAll(PageRequest.of(pageAndSortDTO.getPage(), pageAndSortDTO.getPageSize(), Sort.by(pageAndSortDTO.getSortFieldName()))).toList();
        return mapper.map(teacherList, new TypeToken<List<TeacherGetDTO>>() {
        }.getType());
    }


    public List<TeacherGetDTO> findByIdList(List<UUID> uuidList) {

        List<TeacherGetDTO> teacherGetDTOList = new ArrayList<TeacherGetDTO>();
        Optional<Teacher> op;

        for (UUID id : uuidList) {
            op = teacherRepository.findById(id);
            if (op.isEmpty()) {
                throw new EdAsException("UUID \""+ id.toString() + "\": Пользователь с данным идентификатором не найден.");
            }
            teacherGetDTOList.add(mapper.map(op.get(), TeacherGetDTO.class));
        }

        return teacherGetDTOList;
    }


    public UUID save(TeacherSaveDTO teacher) {

        if (teacher == null) {
            throw new EdAsException("Ввод не распознан.");
        }
        validator.checkValidityOfTeacherSaveDTO(teacher);

        Teacher tempTeacher = mapper.map(teacher, Teacher.class);
        return teacherRepository.save(tempTeacher).getId();
    }


    public TeacherGetDTO findById(UUID id) {
        Optional<Teacher> op = teacherRepository.findById(id);
        if (op.isEmpty()) {
            throw new EdAsException("UUID \""+ id.toString() + "\": Пользователь с данным идентификатором не найден.");
        }
        return mapper.map(op.get(), TeacherGetDTO.class);
    }


    public void deleteById(UUID uuid) {
        teacherRepository.deleteById(uuid);
    }


    public void update(TeacherSaveDTO teacherSaveDTO) {

        validator.checkValidityOfTeacherSaveDTO(teacherSaveDTO);

        Optional<Teacher> op = teacherRepository.findById(teacherSaveDTO.getId());
        if (op.isEmpty())
            throw new EdAsException("UUID \""+ teacherSaveDTO.getId().toString() + "\": Пользователь с данным идентификатором не найден.");
        mapper.map(teacherSaveDTO, op.get());
        teacherRepository.save(op.get());
    }


    public List<TeacherGetDTO> findAllBySubject(String subject) {

        List<Teacher> teacherList = teacherRepository.findDistinctAllByGradesAssigned_SubjectIgnoreCase(subject);
        return mapper.map(teacherList, new TypeToken<List<TeacherGetDTO>>() {
        }.getType());
    }


    public List<TeacherGetDTO> findSubjectsById(String subject) {

        List<Teacher> teacherList = teacherRepository.findDistinctAllByGradesAssigned_SubjectIgnoreCase(subject);
        return mapper.map(teacherList, new TypeToken<List<TeacherGetDTO>>() {
        }.getType());
    }


    private List<TeacherGetDTO> getDTOListFromEntities(List<Teacher> teacherList) {
        List<TeacherGetDTO> teacherGetDTOList = new ArrayList<>();
        for (Teacher t : teacherList) {
            teacherGetDTOList.add(mapper.map(t, TeacherGetDTO.class));

        }
        return teacherGetDTOList;
    }

}
