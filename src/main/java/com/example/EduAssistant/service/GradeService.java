package com.example.EduAssistant.service;

import com.example.EduAssistant.dto.Grade.GradeDTO;
import com.example.EduAssistant.dto.PageAndSortDTO;
import com.example.EduAssistant.entity.Grade;
import com.example.EduAssistant.exceptions.EdAsException;
import com.example.EduAssistant.repository.GradeRepository;
import com.example.EduAssistant.repository.StudentRepository;
import com.example.EduAssistant.repository.TeacherRepository;
import com.example.EduAssistant.sInterface.GradeInterface;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class GradeService implements GradeInterface {

    private final TeacherRepository teacherRepository;
    private final StudentRepository studentRepository;
    private final GradeRepository gradeRepository;
    private final ModelMapper mapper;
    private final ValidityChecker validator;


    public GradeDTO findById(UUID id) {
        Optional<Grade> op = gradeRepository.findById(id);
        if (op.isEmpty()) {
            throw new EdAsException("UUID \""+ id.toString() + "\": Пользователь с данным идентификатором не найден.");
        }
        return convertEntityToDTO(op.get());
    }

    public List<GradeDTO> findAll(PageAndSortDTO pageAndSortDTO) {
        List<Grade> gradeList = gradeRepository.findAll(PageRequest.of(pageAndSortDTO.getPage(), pageAndSortDTO.getPageSize(), Sort.by(pageAndSortDTO.getSortFieldName()))).toList();
        return getDTOListFromEntities(gradeList);
    }

    private GradeDTO convertEntityToDTO(Grade grade) {
        GradeDTO tempGradeDTO = mapper.map(grade, GradeDTO.class);
        tempGradeDTO.setStudentReceiver(grade.getStudentReceiver().getId());
        tempGradeDTO.setTeacherAssigner(grade.getTeacherAssigner().getId());
        return tempGradeDTO;
    }

    private Grade convertDTOToEntity(GradeDTO gradeDTO) {
        Grade tempGrade = mapper.map(gradeDTO,Grade.class);
        tempGrade.setStudentReceiver(studentRepository.getById(gradeDTO.getStudentReceiver()));
        tempGrade.setTeacherAssigner(teacherRepository.getById(gradeDTO.getTeacherAssigner()));
        return tempGrade;
    }

    public UUID save(GradeDTO saveGradeDTO) {
        validator.checkValidityOfGradeDTO(saveGradeDTO);
        return gradeRepository.save(convertDTOToEntity(saveGradeDTO)).getId();
    }

    public void update(GradeDTO updateGradeDTO) {
        validator.checkValidityOfGradeDTO(updateGradeDTO);
        Optional<Grade> op = gradeRepository.findById(updateGradeDTO.getId());
        if (op.isEmpty()) {
            throw new EdAsException("UUID \""+ updateGradeDTO.getId().toString() + "\": Запись с данным идентификатором не найдена.");
        }
        mapper.map(updateGradeDTO,op.get());
        op.get().setTeacherAssigner(teacherRepository.getById(updateGradeDTO.getTeacherAssigner()));
        op.get().setStudentReceiver(studentRepository.getById(updateGradeDTO.getStudentReceiver()));

        gradeRepository.save(op.get());
    }

    public void deleteById(UUID id) {
        gradeRepository.deleteById(id);
    }

    public List<GradeDTO> findAllByGrade(int grade) {
        List<Grade> gradeList = gradeRepository.findAllByGrade(grade);
        return getDTOListFromEntities(gradeList);
    }

    public List<GradeDTO> findAllBySubject(String subject) {
        List<Grade> gradeList = gradeRepository.findAllBySubjectIgnoreCase(subject);
        return getDTOListFromEntities(gradeList);
    }

    private List<GradeDTO> getDTOListFromEntities(List<Grade> gradeList) {
       //return gradeList.parallelStream().map(this::convertEntityToDTO).collect(Collectors.toList());
        List<GradeDTO> gradeDTOList = new ArrayList<>();
        for (Grade g : gradeList) {
            gradeDTOList.add(convertEntityToDTO(g));
        }
        return gradeDTOList;
    }
}
