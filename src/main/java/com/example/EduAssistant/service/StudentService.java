
package com.example.EduAssistant.service;

//import com.example.EduAssistant.dto.ResponseObject;

import com.example.EduAssistant.dto.PageAndSortDTO;
import com.example.EduAssistant.dto.student.StudentGetDTO;
import com.example.EduAssistant.dto.student.StudentSaveDTO;
import com.example.EduAssistant.entity.Student;
import com.example.EduAssistant.exceptions.EdAsException;
import com.example.EduAssistant.repository.StudentRepository;
import com.example.EduAssistant.sInterface.StudentInterface;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class StudentService implements StudentInterface {

    private final ModelMapper mapper;
    private final  StudentRepository studentRepository;
    private final ValidityChecker validator;


    public List<UUID> save(List<StudentSaveDTO> studentsList) {

        if ((studentsList == null ) || (studentsList.size()==0)) throw new EdAsException("Пользовательский ввод не распознан как список студентов.");
        List<UUID> uuidList = new ArrayList<>();
        Student tempStudent;
        for (StudentSaveDTO t : studentsList){
            validator.checkValidityOfStudentSaveDTO(t);
            tempStudent = mapper.map(t, Student.class);
            studentRepository.save(tempStudent);
            uuidList.add(tempStudent.getId());
        }
        return uuidList;
    }


    public List<StudentGetDTO> findAll(PageAndSortDTO pageAndSortDTO)
    {
        List<Student> studentList = (studentRepository.findAll(PageRequest.of(pageAndSortDTO.getPage(), pageAndSortDTO.getPageSize(), Sort.by(pageAndSortDTO.getSortFieldName())))).getContent();
        return getDTOListFromEntities(studentList);
    }


    public List<StudentGetDTO> findAllLazy()
    {
        return mapper.map(studentRepository.findAll(),new TypeToken<List<StudentGetDTO>>(){}.getType());
    }


    public List<StudentGetDTO> findByIdList(List<UUID> uuidList){

        List <StudentGetDTO> studentGetDTOList= new ArrayList<StudentGetDTO>();
        Optional<Student> op;

        for (UUID id : uuidList) {
            op = studentRepository.findById(id);
            if (op.isEmpty()) {
                throw new EdAsException("UUID \""+ id.toString() + "\": Пользователь с данным идентификатором не найден.");
            }
            studentGetDTOList.add(mapper.map(op.get(), StudentGetDTO.class));
        }

        return studentGetDTOList;
    }


    public UUID save(StudentSaveDTO student){

        validator.checkValidityOfStudentSaveDTO(student);

        Student tempStudent = mapper.map(student, Student.class);
        return studentRepository.save(tempStudent).getId();
    }


    public StudentGetDTO findById(UUID id){
        Optional<Student> op = studentRepository.findById(id);
        if (op.isEmpty()) {
            throw new EdAsException("UUID \""+ id.toString() + "\": Пользователь с данным идентификатором не найден.");
        }
        return mapper.map(op.get(), StudentGetDTO.class);
    }


    public void deleteById(UUID uuid){
        studentRepository.deleteById(uuid);
    }


    public void update(StudentSaveDTO studentSaveDTO){

        validator.checkValidityOfStudentSaveDTO(studentSaveDTO);

        Optional<Student> op = studentRepository.findById(studentSaveDTO.getId());
        if (op.isEmpty())
            throw new EdAsException("UUID \""+ studentSaveDTO.getId().toString() + "\": Пользователь с данным идентификатором не найден.");

        mapper.map(studentSaveDTO,op.get());

        studentRepository.save(op.get());
    }


    public List<StudentGetDTO> findAllExcellentStudents() {

        List<Student> studentList = studentRepository.findAllExcellentStudents();
        return mapper.map(studentList,new TypeToken<List<StudentGetDTO>>(){}.getType());
    }


    public List<StudentGetDTO> findAllUnderachievingStudents() {

        List<Student> studentList = studentRepository.findAllUnderachievingStudents();
        return mapper.map(studentList,new TypeToken<List<StudentGetDTO>>(){}.getType());
    }


    private List<StudentGetDTO> getDTOListFromEntities (List<Student> studentList){
        List<StudentGetDTO> studentGetDTOList=  new ArrayList<>();
        for (Student s : studentList){
            studentGetDTOList.add(mapper.map(s,StudentGetDTO.class));
        }
        return studentGetDTOList;
    }

}
