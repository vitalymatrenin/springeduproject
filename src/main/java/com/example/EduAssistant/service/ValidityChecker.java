package com.example.EduAssistant.service;

import com.example.EduAssistant.dto.Grade.GradeDTO;
import com.example.EduAssistant.dto.student.StudentSaveDTO;
import com.example.EduAssistant.dto.teacher.TeacherSaveDTO;
import com.example.EduAssistant.exceptions.EdAsException;
import com.example.EduAssistant.repository.StudentRepository;
import com.example.EduAssistant.repository.TeacherRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class ValidityChecker {

    private final  ModelMapper mapper;
    private final  TeacherRepository teacherRepository;
    private final  StudentRepository studentRepository;

    boolean isStringValid(String string, int maxSize) {
        if ((string == null) || (string.isEmpty())) {
            return false;
        } else if (string.length() > maxSize) {
            return false;
        } else {
            return true;
        }
    }

    public void checkValidityOfTeacherSaveDTO(TeacherSaveDTO teacher) {

        if (teacher == null) {
            throw new EdAsException("Ввод не распознан как строка таблицы учителей.");
        }

        if (!isStringValid(teacher.getFirstName(), 20)) {
            throw new EdAsException("Строка \""+ teacher.getFirstName() + "\": Имя отсутствует, содержит недопустимые символы или количество символов в строке превышает максимальное допустимое.");
        }
        System.out.println("firstNameChecked");
        if (!isStringValid(teacher.getSecondName(), 20)) {
            throw new EdAsException("Строка \""+ teacher.getSecondName() + "\": Фамилия отсутствует, содержит недопустимые символы или количество символов в строке превышает максимальное допустимое.");
        }
        System.out.println("secondNameChecked");
        if ((teacher.getLastName() != null) && (teacher.getLastName().length() > 20)) {
            throw new EdAsException("Строка \""+ teacher.getLastName() + "\": Отчество содержит недопустимые символы или количество символов в строке превышает максимальное допустимое.");
        }
        if (!isStringValid(teacher.getLogin(), 30)) {
            throw new EdAsException("Строка \""+ teacher.getLogin() + "\": Логин отсутствует, содержит недопустимые символы или количество символов в строке превышает максимальное допустимое.");
        }
        if (!isStringValid(teacher.getPassword(), 30)) {
            throw new EdAsException("Строка \""+ teacher.getPassword() + "\": Пароль отсутствует, содержит недопустимые символы или количество символов в строке превышает максимальное допустимое.");
        }
        if (teacher.getDateOfBirth() == null) {
            throw new EdAsException("Дата отсутствует.");
        }
    }


    public void checkValidityOfStudentSaveDTO(StudentSaveDTO student) {

        if (student == null) {
            throw new EdAsException("Ввод не распознан как строка таблицы студентов.");
        }

        if (!isStringValid(student.getFirstName(), 20)) {
            throw new EdAsException("Строка \""+ student.getFirstName() + "\": Имя отсутствует, содержит недопустимые символы или количество символов в строке превышает максимальное допустимое.");
        }
        if (!isStringValid(student.getSecondName(), 20)) {
            throw new EdAsException("Строка \""+ student.getSecondName() + "\": Фамилия отсутствует, содержит недопустимые символы или количество символов в строке превышает максимальное допустимое.");
        }
        if ((student.getLastName() != null) && (student.getLastName().length() > 20)) {
            throw new EdAsException("Строка \""+ student.getLastName() + "\": Отчество содержит недопустимые символы или количество символов в строке превышает максимальное допустимое.");
        }
        if (!isStringValid(student.getLogin(), 30)) {
            throw new EdAsException("Строка \""+ student.getLogin() + "\": Логин отсутствует, содержит недопустимые символы или количество символов в строке превышает максимальное допустимое.");
        }
        if (!isStringValid(student.getPassword(), 30)) {
            throw new EdAsException("Строка \""+ student.getPassword() + "\": Пароль отсутствует, содержит недопустимые символы или количество символов в строке превышает максимальное допустимое.");
        }
        if (student.getDateOfBirth() == null) {
            throw new EdAsException("Дата отсутствует.");
        }
    }


    public void checkValidityOfGradeDTO(GradeDTO grade) {

        if (grade == null) {
            throw new EdAsException("Ввод не распознан как строка таблицы оценок.");
        }

        if (!isStringValid(grade.getSubject(), 40)) {
            throw new EdAsException("Строка \""+ grade.getSubject() + "\": Название дисциплины отсутствует, содержит недопустимые символы или количество символов в строке превышает максимальное допустимое.");
        }
        if (grade.getEntryDate() == null) {
            throw new EdAsException("Дата отсутствует.");
        }
        if (grade.getGrade() < 0) {
            throw new EdAsException("Оценка меньше нуля.");
        }

        if (grade.getTeacherAssigner() == null) {
            throw new EdAsException("Поле \""+ grade.getTeacherAssigner() + "\": Ошибка в поле ID учителя");
        }

        if (!teacherRepository.existsById(grade.getTeacherAssigner())) {
            throw new EdAsException("Поле \""+ grade.getTeacherAssigner() + "\": Учитель с указанным ID отсутствует.");
        }

        if (grade.getStudentReceiver() == null) {
            throw new EdAsException("Поле \""+ grade.getStudentReceiver() + "\": Ошибка в поле ID ученика");
        }

        if (!studentRepository.existsById(grade.getStudentReceiver())) {
            throw new EdAsException("Поле \""+ grade.getStudentReceiver() + "\": Ученик с указанным ID отсутствует.");
        }
    }
}
