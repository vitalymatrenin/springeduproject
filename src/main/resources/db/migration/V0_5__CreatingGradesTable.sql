CREATE TABLE test.studentGrades(
id UUID PRIMARY KEY,
entry_date DATE NOT NULL,
grade INT NOT NULL,
subject VARCHAR(40) NOT NULL,
teacher_id UUID NOT NULL,
student_id UUID NOT NULL
);