TRUNCATE TABLE test.studentgrades, test.students, test.teachers;

INSERT INTO test.students VALUES
    ('Alfreda','Colon','Montoya','2009-12-24','9e467f6d-eced-4206-8006-2c222d6e08f3','PBHXXP_548','lgih3565'),
    ('Amery','Gill','Townsend','2007-05-21','1374e513-3353-4407-bcf7-3e74a7f3f659','IOFDBP_623','lcej4530'),
    ('Kiara','Sears','Richard','2008-05-04','1df2d99f-1178-424f-bcfa-ebc3e3c5fe6c','XBLKYT_984','krwp8815'),
    ('Althea','Hurst','','2011-10-09','462afe73-0c95-4f7e-92c0-70daba5e2559','QUEUTM_762','tnxf3124'),
    ('Finn','Noel','','2013-10-10','f04981d8-ac1f-4d0b-a88a-c44658c1e0a4','FXVMPJ_594','pjef4515'),
    ('Jacob','Butler','','2012-01-11','600c066d-4e01-4c32-ae32-4629fb5ac19d','FLZITO_785','qdcj1053'),
    ('Thomas','Rowland','','2006-02-27','47c4d5bd-12f2-443a-95eb-55ae83f3689b','RQMCBR_878','clmj1137'),
    ('Ryan','Mullen','Perkins','2012-03-22','ea6e14e2-6b25-40bf-829a-b575dee641cc','MGEIXC_297','ycqg9977'),
    ('Heidi','Bush','','2006-12-12','aedd7805-9410-4c32-9129-bc4ce00b26d4','YEULPB_342','udnj1686'),
    ('Owen','Cobb','Finch','2009-11-13','6735da34-5c86-4f45-97d4-ffb1e777817e','HBRLTN_877','jfjv5924');

INSERT INTO test.students VALUES
    ('May','Chen','','2009-04-14','8b19373d-2b6d-45a7-837f-b09786323bff','AVTCRG_688','mwod1433'),
    ('Peter','Powers','Craig','2011-09-15','e3d3b86f-3b60-4fd6-9314-9b8c54587882','MBYBNC_977','nynz2535'),
    ('Shad','Watson','Contreras','2010-07-25','8ec0ec1e-91b8-48a7-aef2-ff66088e327a','CLLYFP_265','dexb3839'),
    ('Amir','Wright','Kane','2006-10-23','cdc4a678-607e-496c-887f-ea21b2941671','URPSTT_212','bbvu0072'),
    ('Rana','Dunn','Drake','2015-09-03','faa75c16-94ec-4e9d-9cd8-eb1c6f986cca','OQQAYX_927','bief0665');

INSERT INTO test.teachers (first_name,second_name,last_name,date_of_birth,id,login,"password") VALUES
	 ('Carla','Clements','Hendrix','1988-04-23','9e711fb5-72d5-4ac0-8910-ce3f4d661848','UDZNJT_714','ebwb8879'),
	 ('Desirae','Black','','1987-08-29','fe40eed8-69f7-435f-87e8-16bf047bc642','JVNEED_242','pycd1546'),
	 ('Conan','Strickland','Carroll','1985-12-18','a3dbcf7d-5af8-45b5-9143-6aa20e21fa23','BBQEKB_653','onue5432'),
	 ('Alfreda','Colon','Montoya','1988-02-04','ece6e769-97ca-4543-b590-a13de2456f90','PBHXXP_548','lgih3565'),
	 ('Amery','Grill','Townsend','1979-05-10','fda02fb1-bb97-4d27-a924-0b97e215e9f8','fdgg33234','6156gfd3'),
	 ('Angelica','Cortez','','2012-05-19','7dc966a2-0bf9-4877-8bb4-ff7528cfa999','SWNIBM_485','xvgw7427'),
	 ('Rhiannon','Cooke','','2011-11-08','3fd7eed3-6c3a-426f-adee-2c934929c703','UBJIRU_225','rnio3764'),
	 ('Juliet','Washington','Vazquez','2007-10-22','dd08302a-64f0-4d6d-b071-77a846a45a6b','WUIUHL_498','hwmh1357'),
	 ('Wyatt','Marsh','Vargas','2006-08-18','cc1c60b2-4d3d-4c63-abb4-08bee125939c','RUXBVL_324','nyfd6338');


INSERT INTO test.studentgrades (id,entry_date,grade,subject,teacher_id,student_id) VALUES
	 ('debee77a-609c-4c2a-9380-3ec82cec8f49','2020-04-12',5,'Rocket science','a3dbcf7d-5af8-45b5-9143-6aa20e21fa23','ea6e14e2-6b25-40bf-829a-b575dee641cc'),
	 ('d79eb0b1-ab54-4f90-8e20-e0e01201d6e0','2020-07-16',2,'English','9e711fb5-72d5-4ac0-8910-ce3f4d661848','1df2d99f-1178-424f-bcfa-ebc3e3c5fe6c'),
	 ('bcafb7ad-60f0-4f6a-9404-be1677f0e64f','2020-05-27',4,'Economics','a3dbcf7d-5af8-45b5-9143-6aa20e21fa23','1df2d99f-1178-424f-bcfa-ebc3e3c5fe6c'),
	 ('62228c61-7f2a-4af8-9766-3838bf116418','2020-09-29',4,'Economics','a3dbcf7d-5af8-45b5-9143-6aa20e21fa23','1df2d99f-1178-424f-bcfa-ebc3e3c5fe6c'),
	 ('6c3874e5-a31b-44a2-8c90-01461cc1861b','2020-09-24',3,'Physical education','dd08302a-64f0-4d6d-b071-77a846a45a6b','6735da34-5c86-4f45-97d4-ffb1e777817e'),
	 ('eaf4ab49-0093-4a34-b49a-58a603096ea7','2020-09-24',3,'Physical education','dd08302a-64f0-4d6d-b071-77a846a45a6b','9e467f6d-eced-4206-8006-2c222d6e08f3'),
	 ('c783c151-6be8-4dd7-ba00-5e8e4e064677','2020-09-24',4,'Physical education','dd08302a-64f0-4d6d-b071-77a846a45a6b','1374e513-3353-4407-bcf7-3e74a7f3f659'),
	 ('aad2c8b5-d776-4f03-8b3e-51c0946249b9','2020-09-24',4,'Physical education','dd08302a-64f0-4d6d-b071-77a846a45a6b','1df2d99f-1178-424f-bcfa-ebc3e3c5fe6c'),
	 ('30d7ed70-b254-4b5b-a705-65afdca399a0','2020-09-24',5,'Physical education','dd08302a-64f0-4d6d-b071-77a846a45a6b','faa75c16-94ec-4e9d-9cd8-eb1c6f986cca'),
	 ('0e1b2f37-aa11-4600-b409-b6914a14f8a3','2020-09-24',5,'Chemistry','fe40eed8-69f7-435f-87e8-16bf047bc642','ea6e14e2-6b25-40bf-829a-b575dee641cc');
INSERT INTO test.studentgrades (id,entry_date,grade,subject,teacher_id,student_id) VALUES
	 ('b2ab4e14-9807-4aa1-a21e-88c7ebe67061','2020-09-24',2,'Chemistry','fe40eed8-69f7-435f-87e8-16bf047bc642','462afe73-0c95-4f7e-92c0-70daba5e2559'),
	 ('4b28b9e9-9509-46a2-8660-48e4a8b7b70a','2020-09-24',5,'Chemistry','fe40eed8-69f7-435f-87e8-16bf047bc642','e3d3b86f-3b60-4fd6-9314-9b8c54587882'),
	 ('2997702b-d933-4ee7-acc4-f39241e89050','2022-02-18',5,'English','9e711fb5-72d5-4ac0-8910-ce3f4d661848','1df2d99f-1178-424f-bcfa-ebc3e3c5fe6c');

