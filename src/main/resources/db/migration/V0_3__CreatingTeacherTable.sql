CREATE TABLE test.teachers(
first_name VARCHAR(20) NOT NULL,
second_name VARCHAR(20) NOT NULL,
last_name VARCHAR(20),
date_of_birth DATE NOT NULL,
id UUID PRIMARY KEY,
login VARCHAR(30),
password VARCHAR(30)
);
