ALTER TABLE test.studentgrades
    ADD CONSTRAINT studkey FOREIGN KEY (student_id) REFERENCES test.students (id);

ALTER TABLE test.studentgrades
    ADD CONSTRAINT teachkey FOREIGN KEY (teacher_id) REFERENCES test.teachers (id);