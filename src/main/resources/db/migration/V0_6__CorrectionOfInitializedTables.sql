ALTER TABLE test.teachers
    ALTER COLUMN login SET NOT NULL,
    ALTER COLUMN password SET NOT NULL;

ALTER TABLE test.students
    ALTER COLUMN login SET NOT NULL,
    ALTER COLUMN password SET NOT NULL;

